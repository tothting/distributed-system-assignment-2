/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author pattanan
 */
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class Assignment2 {
    public static class TokenizerMapper extends Mapper<Object, Text, Text, Text>{

        private final static IntWritable one = new IntWritable(1);
        private Text word = new Text();

        public void map(Object key, Text value, Context context
        ) throws IOException, InterruptedException {
            StringTokenizer itr = new StringTokenizer(value.toString());
            while (itr.hasMoreTokens()) {
                String[] string = itr.nextToken().split(",");
                String filedOfDegree = string[83];
                String occupation = string[98];
                word.set(filedOfDegree);
                context.write(word,new Text(occupation));
                System.out.println("Mapper: "+filedOfDegree+", "+occupation);
            }
        }
    }

    public static class IntSumReducer extends Reducer<Text,Text,Text,Text> {

        //        private MapWritable result = new MapWritable();
        //        Iterable<Map<String,IntWritable>>
        public void reduce(Text key, Iterable <Text> values, Context context) throws IOException, InterruptedException {

            int sum = 0;
            String occupation = "";

            for(Text value : values){
                occupation = value.toString();
                sum += 1;
            }
//            result.put(new Text(occupation),new Text(""+sum));
            String result = "{"+occupation+","+sum+"}";
            context.write(key, new Text(""+result));
            System.out.println("Reducer: "+key.toString()+",{"+occupation+"}");
        }
    }

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "word count");
        job.setJarByClass(Assignment2.class);
        job.setMapperClass(TokenizerMapper.class);
        job.setCombinerClass(IntSumReducer.class);
        job.setReducerClass(IntSumReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}